import java.util.Scanner;

public class Cube {

    public static void main(String[] args) {
//        System.out.println("Enter an edge length: ");
//
//        Scanner scanner = new Scanner(System.in);
//        double edge;
//        while (!scanner.hasNextDouble()) {
//            System.out.println("Enter a number!");
//            scanner.next();
//        }
//        edge = scanner.nextDouble();
//
//        System.out.println("Volume of cube with edge = " + edge + " is equal to: " + calculateVolume(edge));
    }

    public double calculateVolume(int edge) {
        return edge * edge * edge;
    }

}
