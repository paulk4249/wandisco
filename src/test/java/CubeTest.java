import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class CubeTest {
    @Test
    public void cubeTestOne() {
        Cube c = new Cube();
        int volume = (int) c.calculateVolume(1);

        Assert.assertThat(volume, CoreMatchers.is(1));
    }

    @Test
    public void cubeTestTwo() {
        Cube c = new Cube();
        int vol = (int) c.calculateVolume(5);

        Assert.assertThat(vol, CoreMatchers.equalTo(125));
    }
}
